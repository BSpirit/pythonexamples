
class Bidon:

    zaz = "je suis un pro du python"

    def __init__(self, txt: str, num: int = 42, **kwargs):
        self.txt = txt
        self.num = num
        for key, value in kwargs.items():
            setattr(self, key, value)


def var2listsort(*args):
    return sorted(args)


if __name__ == "__main__":
    print(Bidon.zaz)
    a = Bidon("cool")
    print(vars(a))
    a = Bidon("heu", 666)
    print(a.num)
    a = Bidon("keywords var", 1024, le=4, nain=5, a=6, chausette=7)
    print(a.le + a.nain + a.a + a.chausette)
    print(var2listsort(3, 1, 7, 2))
    print(var2listsort(40.3, 40.0, 39.8, 38.7, 39.9, 40.1, 38.80, 38.69))

