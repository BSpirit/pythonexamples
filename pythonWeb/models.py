from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    __tablename__ = 'User'
    id = Column(Integer, primary_key=True)
    name = Column(String(20))
    lastname = Column(String(20))
    login = Column(String(20))
    desc = Column(Text())

    def __repr__(self):
        return ('User(id={id}, name={name!r}, lastname={lastname!r} login={login!r},'
            + ' desc={desc!r})').format(**vars(self))
