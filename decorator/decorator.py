from functools import wraps
import inspect
import itertools


def checktypes(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        args_dict = dict(zip(inspect.getfullargspec(f)[0], args))
        # Checking args type
        for arg, expected_type in f.__annotations__.items():
            if arg != "return" and not isinstance(args_dict[arg], expected_type):
                raise Exception("{}: wrong type of '{}' argument, '{}' expected, got '{}'".format(
                    f.__name__,
                    arg,
                    expected_type.__name__,
                    type(args_dict[arg]).__name__
                ))
        # Checking return type
        res = f(*args, *kwargs)
        if "return" in f.__annotations__.keys() and not isinstance(res, f.__annotations__["return"]):
            raise Exception("{}: wrong return type, {} expected, got {}".format(
                f.__name__,
                f.__annotations__["return"].__name__,
                type(res).__name__
            ))
        return res
    return wrapper
