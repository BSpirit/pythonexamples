
import csv
import os

class CSVFile():
    def __init__(self, filepath: str):
        self._filepath = filepath
        self._header = []
        self._rows = []
        if not os.path.exists(self._filepath):
            with open(self._filepath, 'w'): pass

    def set_header(self, header: [str]):
        self._header = header

    def get_header(self) -> [str]:
        return self._header

    def add_row(self, **kw):
        self._rows.append(kw)

    def get_rows(self) -> [{str: str}]:
        return self._rows

    def load(self):
        try:
            with open(self._filepath, newline='') as csvfile:
                reader = csv.DictReader(csvfile)
                self._header = reader.fieldnames
                self._rows = []
                for row in reader:
                    self._rows.append(row)
        except FileNotFoundError:
            print("No CSV file associated. Please save state first.")

    def save(self):
        with open(self._filepath, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=self._header)
            writer.writeheader()
            for row in self._rows:
                writer.writerow(row)
