import random
import itertools

### 

class AddExpr:
    def __init__(self, lhs, rhs):
        self.lhs = lhs
        self.rhs = rhs
        self.result = 0

    def __repr__(self) -> str:
        return repr(self.lhs) + ' + ' + repr(self.rhs)

    def throw(self) -> int:
        if self.result == 0:
            a = self.lhs.throw()
            b = self.rhs.throw()
            self.result = a + b
        return self.result

    def show(self):
        return self.lhs.show() + ', ' + self.rhs.show()

    def roll(self):
        for row in itertools.product(self.lhs.roll(), self.rhs.roll()):
            yield tuple(itertools.chain.from_iterable(elt for elt in row))

class SubExpr:
    def __init__(self, lhs, rhs):
        self.lhs = lhs
        self.rhs = rhs
        self.result = 0

    def __repr__(self) -> str:
        return repr(self.lhs) + ' - ' + repr(self.rhs)

    def throw(self) -> int:
        if self.result == 0:
            a = self.lhs.throw()
            b = self.rhs.throw()
            self.result = a - b
        return self.result

    def show(self):
        return self.lhs.show() + ', ' + self.rhs.show()

    def roll(self):
        for row in itertools.product(self.lhs.roll(), (tuple(map(lambda x: -x, elt)) for elt in self.rhs.roll())):
            yield tuple(itertools.chain.from_iterable(elt for elt in row))

class MulExpr:
    def __init__(self, lhs, rhs):
        self.lhs = lhs
        self.rhs = rhs
        self.dices = [type(self.rhs)() for i in range(0, self.lhs)]
        self.result = 0

    def __repr__(self) -> str:
        return repr(self.lhs) + repr(self.rhs)

    def throw(self) -> int:
        if self.result == 0:
            for dice in self.dices:
                self.result += dice.throw()
        return self.result

    def show(self):
        return ", ".join([dice.show() for dice in self.dices])

    def roll(self):
        rolls = (self.rhs.roll() for i in range(0, self.lhs))
        for row in itertools.product(*rolls):
            yield tuple(itertools.chain.from_iterable(elt for elt in row))

class Pool:

    darkness = lambda x: len([elt for elt in x if elt >= 5]) >= 4

    def __init__(self, op):
        self.op = op

    def throw(self):
        return self.op.throw()

    def show(self):
        return self.op.show()

    def roll(self):
        return self.op.roll()

    def success(self, lamb) -> float:
        l = [row for row in self.roll() if lamb(row)]
        return (len(list(l)) / len(list(self.roll()))) * 100

    def __add__(self, rhs):
        if type(rhs) is int:
            rhs = FrozenDice(rhs)
        return Pool(AddExpr(self, rhs))

    def __radd__(self, lhs):
        if type(lhs) is int:
            lhs = FrozenDice(lhs)
        return Pool(AddExpr(lhs, self))

    def __sub__(self, rhs):
        if type(rhs) is int:
            rhs = FrozenDice(rhs)
        return Pool(SubExpr(self, rhs))

    def __rsub__(self, lhs):
        if type(lhs) is int:
            lhs = FrozenDice(lhs)
        return Pool(SubExpr(lhs, self))

    def __mul__(self, rhs):
        if type(rhs) is int:
            return Pool(MulExpr(rhs, self))

    def __rmul__(self, lhs):
        return self * lhs

    def __repr__(self) -> str:
        return repr(self.op)

class AbstractResult(Pool):
    min = 1
    max = 1
    def __init__(self):
        self.result = 0

    def __repr__(self):
        return type(self).__name__

    def throw(self):
        if self.result == 0:
            self.result = random.randint(type(self).min, type(self).max)
        return self.result

    def show(self):
        if self.result != 0:
            return str(self.result)

    def roll(self):
        return ((x,) for x in range(1, self.max + 1))

    def seed(s):
        random.seed(s)

class FrozenDice(AbstractResult):
    def __init__(self, v):
        self.result = v
        self.min = v
        self.max = v

    def __repr__(self):
        return str(self.result)

    def throw(self):
        return self.result

    def show(self):
        return str(self.result)

    def roll(self):
        yield (self.result,)

####

class D4(AbstractResult):
    max = 4

class D6(AbstractResult):
    max = 6

class D8(AbstractResult):
    max = 8

class D10(AbstractResult):
    max = 10

class D12(AbstractResult):
    max = 12

class D20(AbstractResult):
    max = 20

d4 = D4()
d6 = D6()
d8 = D8()
d10 = D10()
d12 = D12()
d20 = D20()

if __name__ == "__main__":
    p1 = D6() + d6 + 1
    print(repr(p1))
    p2 = d6 * 4 + 12 + 4 * d8
    print(repr(p2))
    AbstractResult.seed(1) # XXXXX sera donné par la correction
    print(p1.throw())
    print(p2.throw())
    print(p1.show())
    print(p2.show())
    if type(p1.roll()).__name__ != 'generator':
        raise TypeError('la methode roll doit retourner un generateur')
    for t in (2 * D6() - d6).roll():
        print(t)
    proba = (D6() * 6).success(lambda x: sum(x) >= 30)
    print("=> check %s%%" % proba)
    proba = (D10() * 5).success(Pool.darkness)
    print("=> check %s%%" % proba)

    # ASK HOW TO HANDLE SUB
