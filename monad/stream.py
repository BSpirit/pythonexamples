# -*- coding: utf-8 -*-

class Stream:
    def __init__(self, data):
        self.data = data

    @staticmethod
    def of(data):
        return Stream(data)

    def map(self, f):
        self.data = map(f, self.data)
        return self

    def filter(self, f):
      self.data = filter(f, self.data)
      return self

    def sort(self):
      self.data = sorted(self.data)
      return self

    def collect(self, collectType):
        return collectType(self.data)


if __name__ == "__main__":
    l = [5, 2, 4, 3, 1]

    lBis = Stream.of(l) \
        .map(lambda x : 2 * x) \
        .filter(lambda x : x > 5) \
        .sort() \
        .collect(list)
        
    print(l)
    print(lBis)
    