
from struct import *


def ushort_uint(buffer):
    return unpack('>HI', buffer)

def buf2latin(buffer):
    # Lenght is represented by the 2 first bytes
    length = unpack_from('>H', buffer, 0)[0]
    # The string itself comes right after (beware: buffer might be longer)
    string = buffer[2:2 + length]
    return length, string.decode('latin_1')

def ascii2buf(*strings):
    output = bytearray()
    output += pack('>I', len(strings))
    for string in strings:
        output += pack('>H', len(string))
        output += string.encode('ascii')

    return output


if __name__ == "__main__":
    print(ushort_uint(b'\x01\x42\x00\x01\x02\x03'))
    print(buf2latin(b'\x00\x04G\xE9g\xe9zzz'))
    print(ascii2buf("I", "like", "the", "game"))