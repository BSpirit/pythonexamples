
from collections import namedtuple
import os

DBInfos = namedtuple('infos', ['dbname', 'dbpath', 'dbfile'])


class NazeDB:

    def __init__(self, dbname=None):
        """constructor
        ouvre la db si elle existe. regarde à partir du répertoire courant
        et ensuite dans ses sous-rep
        si la db n'existe pas, la créer dans le rep courant
        """
        self._dbname = dbname
        self._dbpath = None
        self._dbfile = '.index.db'

        for root, dirs, files in os.walk('.'):
            if os.path.basename(root) == dbname and '.index.db' in files:
                self._dbpath = root

        if self._dbpath is None:
            try:
                os.makedirs(dbname)
            except:
                print("Repertory already exists")
            self._dbpath = "./" + dbname
            with open(os.path.join(self._dbpath, self._dbfile), 'w'): pass


    def open(self, dbname: str):
        """ouvre la DB nommé dbname (et la cherche au même condition que le ctor)"""
        self.__init__(dbname)

    @ property
    def dbinfos(self) -> DBInfos:
        """Retourne le tuple nommé correspondant a la DB"""
        return DBInfos._make([self._dbname, self._dbpath, self._dbfile])

    @ staticmethod
    def listdb() -> [DBInfos]:
        """Retourne une liste de tous les tuples correspondant a toutes les DB
        accessible a partir du repertoire courant (et sous-repertoire).
        """
        dbList = []
        for root, dirs, files in os.walk('.'):
            if '.index.db' in files:
                dbList.append(DBInfos._make([os.path.basename(root), root, '.index.db']))

        return dbList


