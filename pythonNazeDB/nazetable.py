
from nazedb import *
import os
import csv


class NazeTable:

    def __init__(self, tname: str, db: NazeDB):
        """constructor qui prends
        tname : le nom de la table
        db : une instance de NazeDB dans laquel la table est crée
        """
        self._tname = tname
        self._db = db
        self._header = []
        self._rows = []

        # Fill .index.db
        with open(os.path.join(self._db.dbinfos.dbpath, self._db.dbinfos.dbfile), "r+") as f:
            new_line = self._tname + "\n"
            for line in f:
                if new_line == line:
                    break
            else:
                f.write(new_line)
                
        # Create <tname>.csv file if not exists
        if not os.path.exists(self.filepath):
            with open(self.filepath, 'w'): pass

    def set_header(self, ls: [str]):
        """affecte la liste des champs de la table
        ls : liste de str pour chaque champs
        """
        self._header = ls

    def get_header(self) -> [str]:
        """retourne la liste des champs de la table"""
        return self._header

    @property
    def filepath(self) -> str:
        """retourne le chemin complet vers le fichier stockant le contenu
        de la table (incluant le nom du fichier)
        """
        return os.path.join(self._db.dbinfos.dbpath, self._tname + ".csv")

    @property
    def get_rows(self) -> [{str: str}]:
        """retourne l'état interne représentant le contenu de la table
        une liste (ligne) de dict (colonne) de str
        """
        return self._rows

    def add_row(self, **kw):
        """ajoute une ligne dans l'état interne"""
        self._rows.append(kw)

    def save(self):
        """sauve l'état interne dans le fichier miroir de la table"""
        with open(self.filepath, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=self._header)
            writer.writeheader()
            for row in self._rows:
                writer.writerow(row)

    def load(self):
        """charge le fichier miroir de la table dans l'état interne"""
        try:
            with open(self.filepath, newline='') as csvfile:
                reader = csv.DictReader(csvfile)
                self._header = reader.fieldnames if reader.fieldnames else []
                self._rows = []
                for row in reader:
                    self._rows.append(row)
        except FileNotFoundError:
            print("No CSV file associated. Please save state first.")

    def clean(self):
        """supprime le fichier stockant le contenu de la table, et met à jour '.index.db'"""
        try:
            os.remove(self.filepath)
        except FileNotFoundError:
            print("No CSV file associated. Please save state first.")

        with open(os.path.join(self._db.dbinfos.dbpath, self._db.dbinfos.dbfile), "r+") as f:
            lines = f.readlines()
            f.seek(0)
            for line in lines:
                if self._tname + "\n" != line:
                    f.write(line)
            f.truncate()

    @staticmethod
    def cleanall(dbinfos: DBInfos):
        """supprime toutes les tables associés à une DB. '.index.db' existe mais vide"""
        for root, dirs, files in os.walk(dbinfos.dbpath):
            for file in files:
                os.remove(os.path.join(root, file))

        with open(os.path.join(dbinfos.dbpath, dbinfos.dbfile), "w"): pass
